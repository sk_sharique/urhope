from __future__ import print_function

from flask import Flask, render_template, redirect, url_for, request, g
from flask import session, abort, flash, jsonify
from flask_sslify import SSLify
from flask_caching import Cache
from flask_mysqlpool import MySQLPool
import json
import os
import datetime
import pymysql
import requests
import socket
import os.path
import flask
import re
import urllib.request
import logging
import string 
import random
import regex as re






app = Flask(__name__)

app.config['TEMPLATES_AUTO_RELOAD'] = True

hst = "";
m_hst = "";
usr = "";
pwd = "";


# if(ip.startswith("94.237")):
#     hst = "10.2.10.122"
#     m_hst = "10.2.9.157"
#     usr = "root"
#     pwd = "Admin.902.14"
#     app.debug = False
#     config={'CACHE_TYPE': 'redis', 'CACHE_REDIS_URL': 'redis://10.2.2.183:6379/0'}
#     app.config['MYSQL_HOST'] = hst
#     app.config['MYSQL_PORT'] = 3306
#     app.config['MYSQL_USER'] = usr
#     app.config['MYSQL_PASS'] = pwd
#     app.config['MYSQL_DB'] = 'twitics'
#     app.config['MYSQL_POOL_NAME'] = 'mysql_pool'
#     app.config['MYSQL_POOL_SIZE'] = 32
#     app.config['MYSQL_AUTOCOMMIT'] = True
#     #sys.path.append('/root/miniconda2/lib/python2.7/site-packages') # Replace this with the place you installed facebookads using pip
#     #sys.path.append('/root/miniconda2/lib/python2.7/site-packages/facebook_business-3.0.0-py2.7.egg-info') # same as above
#     print("Running in production mode")


# else:
hst = "localhost"
usr = "root"
pwd = ""
app.debug = True
config={'CACHE_TYPE': 'redis', 'CACHE_REDIS_URL': 'redis://localhost:6379/3'}

app.secret_key = os.urandom(12)


def get_db():
     db = pymysql.connect(host='localhost', user='root',
                          passwd='', db="urhope_db", charset='utf8mb4')
     return db



# Route for Base template
@app.route('/')
def base():
    return render_template('home.html')


@app.route('/signup/', methods=['GET', 'POST'])
def signup():

	if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'role' in request.form and 'confirm' in request.form:
		name = request.form['name']
		username = request.form['username']
		password = request.form['password']
		confirmpassword = request.form['confirm']
		pincode = request.form['pincode']
		phone = request.form['phone']
		role = request.form['role']

		print(username, password, confirmpassword, role)
		try:
			db = get_db()
			c = db.cursor()
			c.execute("select username from members where username = %s", (username))
			account = c.fetchone()

			if account:
				flash("Email already exists please try again with another email!")

			else:
				if(password == confirmpassword):
					c.execute("insert into members (name, username, phone, pin, role, password ) values (%s, %s, %s, %s, %s, md5(%s))",(name, username, phone, pincode, role, password))
					db.commit()

					return redirect(url_for('login'))
				else:
					flash("Passwords do not match!")	

		except Exception as e:
			print(e)

		return render_template('register.html')	
	else:    	 
		return render_template('register.html')


@app.route('/login/', methods=['GET', 'POST'])
def login():
	if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
		try:
			username = request.form['username']
			password = request.form['password']
			db = get_db()
			c = db.cursor()
			c.execute("select id,name, username, password, role, phone, pin from members where username = %s and password = md5(%s)", (username, password))
			account = c.fetchone()
			if(account is not None):
				session['logged_in'] = True;
				session["user_id"] = account[0]
				session["username"] = account[2]
				session["name"] = account[1]
				session["role"] = account[4]
				session["pin"] = account[6]
				session["phone"] = account[5]

				return redirect(url_for("home"))
			else:
				flash("Invalid Username or Password")
				return render_template('login.html')	
		except Exception as e:
			print(e)	

		return render_template('login.html')	
	else:
		return render_template('login.html')

@app.route('/logout')
def logout():
    # Remove session data, this will log the user out
   session.pop('logged_in', None)
   session.pop('user_id', None)
   session.pop('username', None)
   # Redirect to login page
   return redirect(url_for('login'))

@app.route('/profile', methods=['GET', 'POST'])
def profile():
	if not session.get('logged_in'):
		return redirect(url_for('login'))
	else:
		if(session["role"] == "v"):
			return render_template('volunteers_profile.html')	
		elif(session["role"] == "n"):
			return render_template('ngo_profile.html')
		else:
			return render_template('admin_profile.html')

@app.route('/edit-profile', methods=['GET', 'POST'])
def edit_profile():
	if not session.get('logged_in'):
		return redirect(url_for('login'))
	else:
		if(session["role"] == "v"):
			return render_template('edit_profile_v.html')	
		elif(session["role"] == "n"):
			return render_template('edit_profile_n.html')
		else:
			return render_template('edit_profile_a.html')

@app.route('/home', methods=['GET', 'POST'])
def home():
	if not session.get('logged_in'):
		return redirect(url_for('login'))
	else:
		if(session["role"] == "v"):
			return render_template('volunteers_home.html')	
		elif(session["role"] == "n"):
			return render_template('ngo_home.html')
		else:
			return render_template('admin_home.html')



@app.route('/update-pro', methods=['GET', 'POST'])
def update_pro():

		uname=session["username"]

		if request.method == 'POST' and 'name' in request.form or 'pin' in request.form or 'phone' in request.form or 'address' in request.form or 'about' in request.form:	
			name = request.form['name']
			pincode = request.form['pin']
			phone = request.form['phone']
			address = request.form['address']
			about = request.form['about']
			# username = request.form['username']
			# email = request.form['email']
			# about = request.form['about']
			
			connect = get_db()
			exe = connect.cursor()
			
			exe.execute("UPDATE members SET name=%s,pin=%s, phone=%s,address=%s,about=%s WHERE username = %s ",(name,pincode,phone,address,about,uname))
			connect.commit()

			return redirect(url_for('home'))		
		else:
			flash("Enter the values in empty fields.")
			return redirect(url_for('edit-profile'))


@app.route('/task', methods=['GET', 'POST'])
def task():
	if request.method=="POST":
		task=request.form['task']
		grp=request.form['grp']
		website=request.form['website']
		phone=request.form['phone']
		no_of_volun=request.form['vol_num']
		pincode=request.form['location']
		task_detail = request.form['task_det']
		about = request.form['about']
		task_type = request.form['t_type']

		connect = get_db()
		exe = connect.cursor()

		exe.execute("insert into tasks (task, group, website, contact_no, no_of_volunteers,location,task_details,about_group,task_type) values (%s, %s, %s, %s, %s,%s, %s, %s, %s))",(task, grp, website, phone, no_of_volun ,pincode,task_detail,about,task_type))
		connect.commit()

		flash("Task has been added")

		return render_template('add_task.html')	

	else:
		flash("Task was not added. Try again")
		return render_template('add_task.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)



